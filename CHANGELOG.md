# changelog

## 0.0.10

- Certificate pipeline update validation 
- Pinned Angular CLI installation to `11.0.5`
- Pinned Node Docker image to v16 due to envelope error https://github.com/facebook/create-react-app/issues/11708
- Updating local gitlab server path 

## 0.0.9

- Test run

## 0.0.8

- Adding `Projects` page to navigation and 2 external links to GitLab and LinkedIn
- Adding fontawesome icon packs for Angular
- Fixed pipeline to deploy to private GitLab instance.

## 0.0.7

- Adding navigation
- Adding projects page
- Fixed CI pipeline to build the Angular Build process

## 0.0.6

- adding remote pipeline to deploy website to S3

## 0.0.5

- Adding `-r` command to zip to grab all the contents of the file.

## 0.0.4

- Refacting to use minimal design.

## 0.0.2

- Redisning the look and feel

## 0.0.1

- Initial release.
- Angular based website deployed using Terraform to an AWS S3 static website.
